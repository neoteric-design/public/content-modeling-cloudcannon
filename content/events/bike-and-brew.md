+++
description = "Visit 3 breweries in a leisurely afternoon — on bike!  This 5k route will include some of Chicago's most innovative breweries. Bring your ID, a helmet, and bike lock. "
end_date_and_time = 2022-05-14T22:00:00Z
location = "Starts at the corner of Clark and Balmoral, in Andersonville, Chicago"
organization = "organizations/chicago-brewery-tours.md"
start_date_and_time = 2022-05-14T19:00:07Z
title = "Bike and Brew"

+++
