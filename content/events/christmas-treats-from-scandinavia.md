+++
description = "Join us for a Glogg wine and appetizers tour — featuring 5 local restaurants and their favorite nordic traditions. "
end_date_and_time = 2021-12-18T03:00:00Z
location = "321 Swedish Avenue"
organization = "organizations/chicago-food-tours.md"
start_date_and_time = 2021-12-18T00:45:40Z
title = "Christmas treats from Scandinavia "

+++
