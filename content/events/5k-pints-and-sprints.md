+++
description = "Back for our second year in a row! Join us on a leisurely 5k run that will sample 3 breweries along the way. Bring your ID, cash, and sense of adventure. "
end_date_and_time = 2022-04-16T21:30:00Z
location = "Start at Foster Beach; bike rack available. "
organization = "organizations/chicago-brewery-tours.md"
start_date_and_time = 2022-04-16T19:00:00Z
title = "5K Pints and Sprints"

+++
