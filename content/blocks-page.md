+++
title = "Blocks page"
description = "blocks page"

[[blocks]]
headline = "Headline"
support_text = "support text"
_template = "hero"

[[blocks]]
headline = "headline "
support_text = "support text"
link = "www.google.com"
_template = "cta"

[[_structures.components.values]]
label = "Hero"

  [_structures.components.values.value]
  headline = ""
  support_text = ""
  _template = "hero"

[[_structures.components.values]]
label = "CTA"

  [_structures.components.values.value]
  headline = ""
  support_text = ""
  link = ""
  _template = "cta"

[_inputs.blocks]
type = "array"

  [_inputs.blocks.options]
  structures = "_structures.components"
  text_key = "name"
  subtext_key = "description"
+++
This is the block page
